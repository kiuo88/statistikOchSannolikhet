package statistikOchSannolikhet;
import java.util.Scanner;

public class Menu {
	public Menu(){
		menu1();
	}
	public void menu1() {
		boolean loop = true;
	Scanner in = new Scanner(System.in);
	 System.out.println("How many numbers ?");
	 int amount = in.nextInt();
	 float[] numbers = new float[amount];
	 for (int i = 0; i < amount; i++)
	 {
		 System.out.println("Give number " + (i+1) + ":");
		 float input = in.nextFloat();
		 numbers[i] = input;
	 }
	 do {
   System.out.println("Ready to math? Enter a number for the option you want to choose \n "
            + "1) Find max value \n "
            + "2) Find min value \n "
            + "3) Find average value \n "
            + "4) Find median value \n "
            + "9) Quit \n");
   Stats st = new Stats();
    switch (in.nextInt()) {
        case 1:
        	float max = st.getMax(numbers);
        	System.out.println("Maxvärdet är: " + max);
  //          Stats.menu1();
            break;
        case 2:
        	float min = st.getMin(numbers);
        	System.out.println("Minvärdet är: " + min);
  //          Stats.menu2();
            break;
        case 3:
        	float average = st.getAverage(numbers);
        	System.out.println("Medeltalet är: " + average);
  //      	Stats.menu3();
        	break;
        case 4:
        	float median = st.getMedian(numbers);
        	System.out.println("Median värdet är: " + median);
  //      	Stats.menu4();
        	break;
        case 9:
            loop = false;
            System.out.println("\n Goodbye! \n");
            break;
        default:
            System.out.println("\n Not a valid option \n");
            break;
    }
} while (loop);
}
}