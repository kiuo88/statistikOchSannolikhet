package statistikOchSannolikhet;

import java.util.Arrays;

public class Stats {
//get max
//get min
//get average
//get median
	public float getMax(float[] numbers)
	{
		float max = numbers[0];
		for(int i = 0; i < numbers.length; i++)
		{
			if(numbers[i] > max)
			{
				max = numbers[i];
			}
		}
		return max;
	}
	public float getMin(float[] numbers)
	{
		float min = numbers[0];
		for(int i = 0; i < numbers.length; i++)
		{
			if(numbers[i] < min)
			{
				min = numbers[i];
			}
		}
		return min;
	}
	public float getAverage(float[] numbers)
	{
		float average = 0;
		for (int i = 0; i < numbers.length; i++)
		{
			average = numbers[i] + average;
		}
		average = average / (numbers.length);
		return average;
	}
	public float getMedian(float[] numbers)
	{
		Arrays.sort(numbers);
		float median = numbers[0];
		int middle = numbers.length/2;
		if (numbers.length%2 == 1)
		{
			median = numbers[middle];
		}
		else
		{
			median = (numbers[middle-1] + numbers[middle]) / 2;
		}
		return median;
	}
}
